var geocoder;
var map;
var markersArray = [];

function placeMarkerAndPanTo(latLng, map) {
  var position = latLng;
  console.log(position);
  while (markersArray.length) {
    markersArray.pop().setMap(null);
  }
  var marker = new google.maps.Marker({
    draggable: true,
    position: latLng,
    map: map,
    title: "Select Your Location!"
  });
  map.panTo(latLng);

  markersArray.push(marker);

  google.maps.event.addListener(marker, 'dragend', function(event) {
    var position = event.latLng;
    console.log(position);
  });
}

function initialize() {
  var map = new google.maps.Map(
    document.getElementById("sidebar_build"), {
      center: new google.maps.LatLng({lat: 21.17277096919513,lng: 72.8353214263916}),
      zoom: 9,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

  map.addListener('click', function(e) {

     // alert(e.latLng.lat());
     // alert(e.latLng.lng());

  $("#heir_lan").val(e.latLng.lat()); //val assign in
  $("#heir_lng").val(e.latLng.lng());

   var geocoder = new google.maps.Geocoder;
    geocodeLatLng(geocoder,e.latLng);
    placeMarkerAndPanTo(e.latLng, map);
   });

   function geocodeLatLng(geocoder,address)
   {
     var latlng = address;
    geocoder.geocode({'location': latlng}, function(results, status)
    {
             if (status === 'OK')
             {
               if (results[0])
               {
                 $("#heir_address").val(results[0].formatted_address);
                 // alert(results[0].formatted_address);
                }
                else
                {
                 alert('No results found');
               }
             }
             else
             {
               alert('Geocoder failed due to: ' + status);
             }
          });
  }
 }
 google.maps.event.addDomListener(window, "load", initialize);
