class DetailsController < ApplicationController
  before_action :set_params,:authenticate_member!

  def index

    # @search = Heir.where("member_id=?",current_member.id).search(params[:q]) // for specific memeber login
    @search = Heir.search(params[:q]) #for all member
    @Record = @search.result
    gmap @Record
  end

  def searchajax
    data= params[:data]
    @products=Heir.where('name LIKE ?', "%#{data}%")
    gmap @products
    respond_to do |format|
      format.html
      format.js
   end
  end

  def new
    @heir= Heir.new
  end

  def create
    @heir = Heir.new(heir_params)
    @heir.valid?
    if @heir.errors.blank?
      @heir.save
      redirect_to dashboard_path
    else
      render :action => "new"
    end
  end

  def veiwlist
    @data=Heir.paginate(:per_page => 5, :page => params[:page])
  end

  def show
    redirect_to indexsearch_path
  end

  def Edit
    @heir = Heir.find(params[:id])
  end

  def Update
    @heir = Heir.find(params[:id])
    if @heir.update_attributes(heir_params)
      redirect_to dashboard_path, :id=>@heir
    else
      render 'Edit'
    end
  end

  def Delete
    Heir.find(params[:id]).destroy
    redirect_to list_path
  end

  private

  def gmap record
    @hash = Gmaps4rails.build_markers(record) do |heir, marker|
        marker.lat heir.lan
        marker.lng heir.lng
        marker.infowindow heir.name
    end
  end

  def set_params
    @heir = Heir.new
  end

    def heir_params
      params.require(:heir).permit(:name.toLowerCase,:address,:dob,:dod,:member_id,:lan,:lng)
    end

end
