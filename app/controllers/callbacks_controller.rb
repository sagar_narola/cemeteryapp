class CallbacksController < Devise::OmniauthCallbacksController

    def github
      @member = Member.from_omniauth(request.env["omniauth.auth"])
      sign_in_and_redirect @member
    end

    def facebook
      @member = Member.from_omniauth(request.env["omniauth.auth"])
      sign_in_and_redirect @member
    end
end
