class HomeController < ApplicationController

  def searchhome
    data= params[:data]
    @products=Heir.where('name LIKE ?', "%#{data}%")
    gmap @products
    respond_to do |format|
      format.html
      format.js
   end
  end


  def dashboard
    data= params[:data]
    @products=Heir.where('name LIKE ?', "%#{data}%")
    gmap @products
    respond_to do |format|
      format.html
      format.js
   end
  end



  private

  def gmap record
    @hash = Gmaps4rails.build_markers(record) do |heir, marker|
        marker.lat heir.lan
        marker.lng heir.lng
        marker.infowindow heir.name
    end
  end


end
