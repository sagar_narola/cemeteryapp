class Member < ApplicationRecord
  has_many :heirs, dependent: :destroy
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,:recoverable, :rememberable, :trackable, :validatable,:omniauthable, :authentication_keys => {email: true, login: false} ,
  :omniauth_providers => [:github ,:facebook]

#  attr_accessor :email, :password, :password_confirmation, :remember_me, :username
validates :username,  presence: true, length: { maximum: 50 }
VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
validates :email, presence: true, length: { maximum: 255 },format: { with: VALID_EMAIL_REGEX },uniqueness: { case_sensitive: false }

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |member|
      member.provider = auth.provider
      member.uid = auth.uid
      member.email = auth.info.email
      puts member.email
      puts "___________________"
      member.password = Devise.friendly_token[0,20]
    end
  end
end

#
#
#
# (43, null, $2a$11$wvLYV5MM6.A3.WL1AMYg0OI7aRydDcvtt8uRlqMWkemsQN6x1XTuu, null, null, null, 1, 2017-01-24 08:55:51.803564, 2017-01-24 08:55:51.803564, ::1, ::1, 2017-01-24 08:55:51.803978, 2017-01-24 08:55:51.803978, null, null, null, null, f, github, 24888013).
# ("email", "encrypted_password", "sign_in_count", "current_sign_in_at", "last_sign_in_at", "current_sign_in_ip", "last_sign_in_ip", "created_at", "updated_at", "provider", "uid") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) RETURNING "id"
