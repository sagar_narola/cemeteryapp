class Heir < ApplicationRecord
  belongs_to :member
  validates :name,  presence: true, length: { maximum: 50 }
  validates :address,  presence: true
end
