Rails.application.routes.draw do


  #devise_for :members, :controllers=>{:registrations=>'registration'}
  root :to =>'home#index'
  get 'dashboard' => 'home#dashboard'
  get 'cemetoryform' => 'details#new'
  post 'cemetoryform' => 'details#create'
  get 'cemetoryform' => 'details#show'
  get '/list', to: 'details#veiwlist'
  get '/edit', to: 'details#Edit'
  patch '/edit', to: 'details#Update'
  get '/delete', to: 'details#Delete'



  get '/about', to: 'home#about'
  get '/contact', to: 'home#contact'

  get 'searchajax',to: 'details#searchajax'
  get 'searchhome',to: 'home#searchhome'

  # get 'adddatamap',to: 'details#adddatamap'


  get 'indexsearch' => 'details#index'
  get 'show' => 'details#show'

  devise_for :members, :controllers => { :omniauth_callbacks => "callbacks" ,:registrations=>'registration'}

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :details do
      collection { post :search, to: 'details#index' }
  end
end
