class CreateHeirs < ActiveRecord::Migration[5.0]
  def change
    create_table :heirs do |t|
      t.string :name
      t.text :address
      t.datetime :dob
      t.datetime :dod
      t.references :member, foreign_key: true

      t.timestamps
    end
  end
end
