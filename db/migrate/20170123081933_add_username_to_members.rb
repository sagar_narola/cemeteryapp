class AddUsernameToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :username, :string
    add_column :members, :address, :string
    add_column :members, :mobaile, :string
    add_column :members, :date_of_birth, :datetime
    add_column :members, :is_female, :boolean, default: false
  end
end
