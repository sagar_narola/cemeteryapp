class EditIsFemaleMembers < ActiveRecord::Migration[5.0]
  def change
    rename_column :members, :is_female, :gender 
  end
end
